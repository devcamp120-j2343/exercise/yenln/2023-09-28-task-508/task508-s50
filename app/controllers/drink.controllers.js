const { model } = require("mongoose");
const drinkModel = require("../model/drinkModel");

const createDrink = async (req, res) => {
    // B1: Chuẩn bị dữ liệu
    const {
        maNuocUong,
        tenNuocUong,
        donGia
    } = req.body;

    // B2: Validate dữ liệu

    if (!maNuocUong && mongoose.Types.ObjectId.isValid(maNuocUong))
        res.status(400).json({
            status: "Bad Request",
            message: "maNuocUong is invalid",
        })
    if (!tenNuocUong)
        res.status(400).json({
            status: "Bad Request",
            message: "tenNuocUong is invalid",
        })
    if (!donGia)
        res.status(400).json({
            status: "Bad Request",
            message: "donGia is invalid",
        })
    // B3: Thao tác với CSDL

    try {
        // B3 - save to database
        var newDrink = {
            _id: new mongoose.Types.ObjectId(),
            maNuocUong,
            tenNuocUong,
            donGia
        }
        const result = await drinkModel.create({newDrink});
        return res.status(201).json({
            message: "Create Drink successful !",
            data: result,
        });
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }

}

const getAllDrink = async (req, res) => {
    res.status(200).json({
        message: "Get All Drinks"
    })
}

const getIdDrink = async (req, res) => {
    var DrinkId = req.params.drinkId;

    res.status(200).json({
        message: "Get Drink by Id " + DrinkId,
    })
}

const updateDrink = async (req, res) => {
    var DrinkId = req.params.drinkId;

    res.status(200).json({
        message: "Update Drink: " + DrinkId,
    })
}

const deleteDrink = async (req, res) => {
    var DrinkId = req.params.drinkId;

    res.status(200).json({
        message: "Delete Drink: " + DrinkId,
    })
}


module.exports = {
    createDrink,
    getAllDrink,
    getIdDrink,
    updateDrink,
    deleteDrink
}
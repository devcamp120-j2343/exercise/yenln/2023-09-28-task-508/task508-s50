const { model } = require("mongoose");
const voucherModel = require("../model/voucherModel");

const createVoucher = (req, res) => {
    res.status(200).json({
        message: "Create Voucher"
    })
}

const getAllVoucher = (req, res) => {
    res.status(200).json({
        message: "Get All Vouchers"
    })
}

const getIdVoucher = (req, res) => {
    var VoucherId = req.params.VoucherId;

    res.status(200).json({
        message: "Get Voucher by Id " + VoucherId,
    })
}

const updateVoucher = (req, res) => {
    var VoucherId = req.params.VoucherId;

    res.status(200).json({
        message: "Update Voucher: " + VoucherId,
    })
}

const deleteVoucher = (req, res) => {
    var VoucherId = req.params.VoucherId;

    res.status(200).json({
        message: "Delete Voucher: " + VoucherId,
    })
}


module.exports = {
    createVoucher,
    getAllVoucher,
    getIdVoucher,
    updateVoucher,
    deleteVoucher
}
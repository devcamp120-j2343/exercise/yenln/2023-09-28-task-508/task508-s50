//import { Express } from "express";
const express = require("express");

const userMiddleware = require("../middlewares/user.middleware");

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
});
 
router.get("/", userMiddleware.getAllUsersMiddlewares,(req, res) => {
    res.json({
        message: "Get all Users"
    })
})

router.post("/", userMiddleware.createUsersMiddlewares,(req, res) => {
    res.json({
        message: "Create a User"
    })
})

router.get("/:UserId", userMiddleware.getDetailUsersMiddlewares,(req, res) => {
    res.json({
        message: "Get detail User"
    })
})

router.put("/:UserId", userMiddleware.updateUsersMiddlewares,(req, res) => {
    res.json({
        message: "update Users"
    })
})

router.delete("/:UserId", userMiddleware.deleteUsersMiddlewares,(req, res) => {
    res.json({
        message: "delete Users"
    })
})

module.exports = router;
//import { Express } from "express";
const express = require("express");

const drinkMiddleware = require("../middlewares/drink.middleware");
const drinkControllers = require("../controllers/drink.controllers");

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
});
 
router.get("/", drinkMiddleware.getAllDrinksMiddlewares, drinkControllers.getAllDrink)

router.post("/", drinkMiddleware.createDrinksMiddlewares,drinkControllers.createDrink)

router.get("/:drinkId", drinkMiddleware.getDetailDrinksMiddlewares, drinkControllers.getIdDrink)

router.put("/:drinkId", drinkMiddleware.updateDrinksMiddlewares, drinkControllers.updateDrink)

router.delete("/:drinkId", drinkMiddleware.deleteDrinksMiddlewares,drinkControllers.deleteDrink)

module.exports = router;
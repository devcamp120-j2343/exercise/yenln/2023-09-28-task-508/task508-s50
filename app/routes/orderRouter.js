//import { Express } from "express";
const express = require("express");

const orderMiddleware = require("../middlewares/order.middleware");

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
});
 
router.get("/", orderMiddleware.getAllOrdersMiddlewares,(req, res) => {
    res.json({
        message: "Get all Orders"
    })
})

router.post("/", orderMiddleware.createOrdersMiddlewares,(req, res) => {
    res.json({
        message: "Create a Order"
    })
})

router.get("/:OrderId", orderMiddleware.getDetailOrdersMiddlewares,(req, res) => {
    res.json({
        message: "Get detail Order"
    })
})

router.put("/:OrderId", orderMiddleware.updateOrdersMiddlewares,(req, res) => {
    res.json({
        message: "update Orders"
    })
})

router.delete("/:OrderId", orderMiddleware.deleteOrdersMiddlewares,(req, res) => {
    res.json({
        message: "delete Orders"
    })
})

module.exports = router;